# CNC Control

This repo contains the cnc control software to use our Shapeoko 2 based CNC machine(s):

- Arduino (running GRBL) connected to RPi
- Basic GRBL web UI running on RPi to use the wood shaper cnc
- Simple web UI to mill basic shapes