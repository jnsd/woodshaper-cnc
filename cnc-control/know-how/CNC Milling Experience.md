# CNC Know-How & Experience

## Calculating CNC Parameters

chip load = feed rate in mm/min  /  (# flutes * spindle speed in rpm)

Example: chip load = 0.008 mm/(round and flute) = 1000 mm/min  /  (4 * 30000 rpm)

**Question**: How does the tool diameter come into this?

### General CNC Cutting Info

- [The Feeds and Speeds of CNC cutting tools](http://www.woodworkingcanada.com/r5/showkiosk.asp?listing_id=4994023&switch_issue_id=57167)
- [Basic Concepts for Beginners](http://www.cnccookbook.com/CCCNCMillFeedsSpeedsBasics.htm)

### Chip Load Values

- http://www.vicious1.com/forum/topic/determining-chip-load/


- http://www.shopbottools.com/mTechShop/files/ChipLoad_inch.pdf

## Miscellaneous

### Reply from a forum

https://forum.aquacomputer.de/selbstgebautes/89057-holz-mit-der-cnc-fr-sen-was-mach-ich-falsch/

"in der regel gilt für holz im geegnsatz zu metall je schneller desto besser. das holz verbrennt bei zu hoher reibung d.h wenn etwas verklemmt oder das werkzeug lange am material reibt. die angesprochene schnittgeschwindigkeit von 50m/s is schon super die wirst du mit deinem dremel oder ner kleinen CNC nich hinbekommen 

(so mal rechnerich: v[schnittgeschwindigkeit]=d[Durchmesser in 
m]*pi*n[Drehfrequenz in 1/min]*1min/60sec[für m/s]---> n=v/(d*pi) n 
bei ca 5mm durchmesser des Wekzeugs bei nem dremel = etwa 500.000 touren
 das heisst das du mit nem dremel bei 30.000/min etwa ne 
schnittgeschwindigkeit von 8m hast ds is dann nich so viel [die 
formatkreissäge bei uns aufer arbeit ha nen werkzeugdurchmesser von 
300mm und ne drehfrequenz von 5000/min das ergibt ne 
schnittgeschwindigkeit von fast 80m/s und trotzdem kann da ne 
schnitstelle schwarz werden wenn man nich aufpasst)) 

das problem ist das die werkzeuge für holz eigentlich einen zu kleinen durchmesser haben. du kannst natürlich trotzdem holz damit bearbeitenes wird aber halt nich so ein schönes ergebnis.

(nochma zu den 50m/s holzfasern bei massivholz reißen etwa mit 40-45m/s 
aus d.h. schnittgeschwindigkeit sollte darüberliegen dann reißt nix 
aus.)"

