# import the FreeCAD library
import os
execfile('import-freecad.py')

from FreeCAD import Base;
import Part;
#from PathScripts import PathProject;

s = Part.Shape();
s.read("cad-objects/box-with-circular-pocket.step");


# ********** Basic Functions **********

def isHorizontal(face):
    return face.BoundBox.ZLength == 0;

def isVertical(face):
    # if vertical, the face's normal vector (orthogonal to the face) is orthogonal to the z-vector
    return face.normalAt(0,0).dot(Base.Vector(0,0,1)) == 0;


# ********** Path Functions **********

# >>> import PathScripts.PathUtils
# >>> import PathScripts.PathLoadTool
# >>> import PathScripts.PathLoadTool as PathLoadTool

# >>> obj = FreeCAD.ActiveDocument.addObject("Path::FeatureCompoundPython", "Job")
# >>> PathScripts.PathJob.ObjectPathJob(obj)
# >>> PathLoadTool.CommandPathLoadTool.Create(obj.Name)
# >>> tl = obj.Group[0]
# >>> tool = Path.Tool()
# >>> tool.Diameter = 5.0
# >>> tool.Name = "Default Tool"
# >>> tool.CuttingEdgeHeight = 15.0
# >>> tool.ToolType = "EndMill"
# >>> tool.Material = "HighSpeedSteel"
# >>> obj.Tooltable.addTools(tool)
# >>> tl.ToolNumber = 1
# >>> obj.ViewObject.Proxy.deleteOnReject = True
# >>> obj.ViewObject.startEditing()


# ********** Utils Functions **********

def print_TopoShape(topo_shape):

    def print_Faces(indent, faces):
        for f in faces:
            print indent + str(f.ShapeType);
            print indent + "  Area         : " + str(f.Area);
            print indent + "  isHorizontal : " + str(isHorizontal(f));
            print indent + "  isVertical   : " + str(isVertical(f));
            print indent + "  BoundBox     : " + str(f.BoundBox);
            print indent + "  normalAt(0,0): " + str(f.normalAt(0,0));

    print "***** " + str(topo_shape.ShapeType) + " *****";
    print "  Volume     : " + str(topo_shape.Volume);
    print "  isClosed   : " + str(topo_shape.isClosed());
    print "  Orientation: " + str(topo_shape.Orientation);
    print "  BoundBox   : " + str(topo_shape.BoundBox);
    print "  Compounds  : " + str(len(topo_shape.Compounds));
    print "  CompSolids : " + str(len(topo_shape.CompSolids));
    print "  Faces      : " + str(len(topo_shape.Faces));
    print_Faces("    ", topo_shape.Faces);
    print "  Shells: " + str(len(topo_shape.Shells));
    print "  Solids: " + str(len(topo_shape.Solids));
    print "  Wires: " + str(len(topo_shape.Wires));
    print "**********"

print_TopoShape(s);


# f = s.Faces[4];
# print_TopoShape(f);

# f = s.Faces[7];
# print_TopoShape(f);