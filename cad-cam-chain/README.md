This project includes applications, scripts, modules and tools to build an automagical chain from a 2.5D/3D object to CNC'able gcode.


## Setup for Docker With Latest FreeCAD Library

1. [Install docker](https://docs.docker.com/engine/getstarted/step_one/)
2. Clone this repo to your home folder: ```~/dev-share/cad-cam-chain``` (it must be this exact folder)
3. Build the docker image with the latest FreeCAD daily linux library
    1. Open terminal and go to folder ```docker-env```
    2. Run ```./build.sh``` and wait. It takes time to download the Ubuntu base image, update the Ubuntu packages and download the FreeCAD library for the first time.
    3. Run ```docker images``` to list all docker images and verify that the image has been built


## Start a Container

1. Start container with: ```docker-env/run.sh```. You are now in a terminal within the started container.
2. Run scripts, e.g. ```python obj2gcode_25d.py```
3. When done, exit the terminal with ```exit``` which will also stop the container
