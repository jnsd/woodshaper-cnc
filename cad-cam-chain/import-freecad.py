# execute this file to import FreeCAD to any python app/script:
# import os
# execfile('import-freecad.py')
#
# Sources:
# embedding FreeCAD in external python apps: http://www.freecadweb.org/wiki/index.php?title=Embedding_FreeCAD
# basic FreeCAD code API doc: http://freecadweb.org/wiki/Category:API

import sys
import os
from sys import platform

sys.path.insert(0, os.path.abspath('./'))


if platform == "linux" or platform == "linux2":
    FREECADPATH = '/usr/lib/freecad-daily/lib/'
elif platform == "darwin":
    FREECADPATH = '/Applications/FreeCAD.app/Contents/lib'
#elif platform == "win32":
#    FREECADPATH = '/Applications/FreeCAD.app/Contents/lib'
else :
    print("Path to FreeCAD library is missing for your " + platform + " platform");
    sys.exit();

sys.path.append(FREECADPATH)

import FreeCAD
