#!/bin/bash

# source for gui on OSX host: https://stackoverflow.com/questions/38686932/how-to-forward-docker-for-mac-to-x11?rq=1

# used to separate different X11 forwarding for OSX and Linux
case "$OSTYPE" in
    solaris*) os_type='solaris' ;;
    darwin*)  os_type='darwin' ;; 
    linux*)   os_type="linux" ;;
    bsd*)     os_type="bsd" ;;
    msys*)    os_type="win" ;;
    *)        os_type='unknown: $OSTYPE'
esac

# add temporary access right for X11 from the docker host's ip, i.e. ip of this computer
if [ $os_type = 'darwin' ]; then
    xhost + $(ipconfig getifaddr en0)
fi

docker run -t -i -e DISPLAY=$(ipconfig getifaddr en0):0 -v ~/dev-share/cnc-opencv:/home/prj-repo -v /tmp/.X11-unix:/tmp/.X11-unix cnc-opencv-dev /bin/bash

