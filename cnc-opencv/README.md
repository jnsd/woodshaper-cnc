This project includes CNC feature prototypes built with OpenCV and Python.


## Setup Docker Development Environment

1. [Install docker](https://docs.docker.com/engine/installation/)
2. Clone this repo to your home(!) folder: ```~/dev-share/cad-cam-chain``` (it must be this exact folder)
3. Build the docker dev environment image which includes Python and OpenCV 
    1. Open terminal and go to folder ```docker-env```
    2. Run ```./build.sh``` and wait. It takes time (only once!) to download the Ubuntu base image, update the Ubuntu packages and install the dev environment.
    3. Run ```docker images``` to list all docker images and verify that the image has been built


## Start Developing

1. Start container with: ```docker-env/run.sh```. You are now in a terminal within the started container.
2. Start coding in an editor on your host system and execute them within the docker container, e.g. ```python3 awesome_cnc_feature.py```
3. When done, exit the terminal with ```exit``` which will also stop the container

