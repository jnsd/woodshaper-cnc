# all below examples are taken from tutorials listed here:
# https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_tutorials.html

import numpy as np
import cv2
from matplotlib import pyplot as plt


img = cv2.imread('images/mdf-pencil-mark-and-milled-groove.jpg')
logo = cv2.imread('images/opencv-logo.png')

rows,cols,channels = logo.shape
roi = img[0:rows, 0:cols]


# === single pixel access ===
# b,g,r = img[100, 100]
# print("b={:d}, g={:d}, r={:d}".format(b, g, r))


# === adding objects+text ===
# img = cv2.rectangle(img,(50,50),(250,250),(190,85,235),5)
# font = cv2.FONT_HERSHEY_SIMPLEX
# cv2.putText(img,'some thing',(50,400), font, 4,(0,0,0),5,cv2.LINE_AA)


# === adding the OpenCV logo to the image ===
# logo_gray = cv2.cvtColor(logo,cv2.COLOR_BGR2GRAY)
# ret, mask = cv2.threshold(logo_gray, 10, 255, cv2.THRESH_BINARY)
# mask_inv = cv2.bitwise_not(mask)
# img_bg = cv2.bitwise_and(roi,roi,mask = mask_inv)
# logo_fg = cv2.bitwise_and(logo,logo,mask = mask)
# dst = cv2.add(img_bg,logo_fg)
# img[0:rows, 0:cols ] = dst


# === performance measuring with Python ===
# e1 = cv2.getTickCount()
# for i in range(5,49,2):
#     img = cv2.medianBlur(img,i)
# e2 = cv2.getTickCount()
# t = (e2 - e1) / cv2.getTickFrequency()
# print("The operation took {:f} seconds".format(t))


# === performance measuring with IPython shell ===
# In [4]: x = 5   

# In [5]: %timeit y = x**2
# 10000000 loops, best of 3: 58.6 ns per loop

# In [6]: %timeit y = x*x 
# 10000000 loops, best of 3: 41.6 ns per loop

# In [9]: z = np.uint8([5])

# In [10]: %timeit y=z*z
# 1000000 loops, best of 3: 412 ns per loop

# performance optimization source: https://wiki.python.org/moin/PythonSpeed/PerformanceTips


# === print all available color conversions ===
# flags = [i for i in dir(cv2) if i.startswith('COLOR_')]
# print(flags)


# === adaptive threshold for varying illumination ===
# img = cv2.imread('images/sudoku-paper.jpg')
# img = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
# img = cv2.medianBlur(img, 5)
# th = cv2.adaptiveThreshold(img, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C,\
#         cv2.THRESH_BINARY, 13, 2)
# see also Otsu's binarization


# === transform tilted square to 'proper' square ===
# img = cv2.imread('images/mdf-perspective-grooves.jpg')
# rows,cols,ch = img.shape

# only upper left corner has 90 degrees:
# pts_src = np.float32([[1096,1028],[448,2680],[2964,1036]])
# pts_dst = np.float32([[448,1028],[448,2680],[2964,1036]])
# M = cv2.getAffineTransform(pts_src,pts_dst)
# dst = cv2.warpAffine(img,M,(cols,rows))

# all corners are 90 degrees:
# pts1 = np.float32([[1096,1028],[2964,1036],[448,2680],[3636,2704]])
# pts2 = np.float32([[200,200],[2740,200],[200,2740],[2740,2740]])
# M = cv2.getPerspectiveTransform(pts1,pts2)
# dst = cv2.warpPerspective(img,M,(3000,3000))

# plt.subplot(1, 2, 1),plt.imshow(img),plt.title('Input')
# plt.subplot(1, 2, 2),plt.imshow(dst),plt.title('Output')
# plt.show()


# === removing noise while preserving edges ===
img = cv2.imread('images/mdf-pencil-mark-and-milled-groove.jpg')
blur = cv2.bilateralFilter(img,9,75,75)
plt.subplot(1, 2, 1), plt.imshow(img)
plt.subplot(1, 2, 2), plt.imshow(blur)
plt.show()

# plt.imshow(blur, cmap = 'gray', interpolation = 'bicubic')
# plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
# plt.show()
