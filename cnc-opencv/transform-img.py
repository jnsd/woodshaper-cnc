import argparse
from transform import four_point_transform
from matplotlib import pyplot as plt
import cv2
import imutils
from imutils import perspective
from imutils import contours


# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", help = "path to the image file")
args = vars(ap.parse_args())

# load input image, convert it to grayscale, and blur it slightly
image = cv2.imread(args["image"])  # use 0 as second parameter to load as gray image
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
gray = cv2.GaussianBlur(gray, (7, 7), 0)

# perform edge detection, then perform a dilation + erosion to close gaps in between object edges
edged = cv2.Canny(gray, 50, 100)
edged = cv2.dilate(edged, None, iterations=1)
edged = cv2.erode(edged, None, iterations=1)

# find contours in the edge map
cnts = cv2.findContours(edged.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
cnts = cnts[0] if imutils.is_cv2() else cnts[1]

# sort the contours from left-to-right and initialize the bounding box point colors
(cnts, _) = contours.sort_contours(cnts)
print(cnts)


#pts = np.array(eval(args["coords"]), dtype = "float32")
 
# apply the four point tranform to obtain a "birds eye view" of the image
#warped = four_point_transform(image, pts)
 
# show the original and warped images
# cv2.imshow("Original", image)
# cv2.imshow("Warped", warped)
# cv2.waitKey(0)

plt.subplot(1, 2, 1), plt.imshow(image)
plt.subplot(1, 2, 2), plt.imshow(edged, cmap = plt.get_cmap('gray'))
plt.show()
