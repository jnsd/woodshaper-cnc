# based on this tutorial: https://www.pyimagesearch.com/2016/03/21/ordering-coordinates-clockwise-with-python-and-opencv/

# import the necessary packages
import numpy as np


# rectangular(!) points ordering
def order_rect_points(pts):
    # sort the points based on their x-coordinates
    x_sorted = pts[np.argsort(pts[:, 0]), :]
    # grab the left-most and right-most points from the sorted x-coordinate points
    left_most = x_sorted[:2, :]
    right_most = x_sorted[2:, :]
    # now, sort the left-most coordinates according to their y-coordinates so we can grab the top-left and
    # bottom-left points, respectively
    left_most = left_most[np.argsort(left_most[:, 1]), :]
    (tl, bl) = left_most
    # now that we have the top-left coordinate, use it as an anchor to calculate the Euclidean distance between
    # the top-left and right-most points; by the Pythagorean theorem, the point with the largest distance will
    # be our bottom-right point
    # faster euclidean distance calc: numpy.linalg.norm(tl-right_most)
    D = dist.cdist(tl[np.newaxis], right_most, "euclidean")[0]
    # array[i:j:n] selects the very n-th element with indices >=i and <j
    # -n selects every n-th element of the reversed array
    (br, tr) = right_most[np.argsort(D)[::-1], :]
    # return the coordinates in top-left, top-right, bottom-right, and bottom-left order
    return np.array([tl, tr, br, bl], dtype="float32")


# ONLY FOR REFERENCE - DO NOT USE!
# rectangular points ordering funtion with two issues:
# 1. points with identical sum can result in wrong ordering
# 2. points with identical difference can result in wrong ordering
def order_rect_points_old(pts):
    # initialize a list of coordinates that will be ordered such that the first entry in the list is the top-left,
    # the second entry is the top-right, the third is the bottom-right, and the fourth is the bottom-left
    rect = np.zeros((4, 2), dtype="float32")
    # the top-left point will have the smallest sum, whereas the bottom-right point will have the largest sum
    s = pts.sum(axis=1)
    rect[0] = pts[np.argmin(s)]
    rect[2] = pts[np.argmax(s)]
    # now, compute the difference between the points, the top-right point will have the smallest difference,
    # whereas the bottom-left will have the largest difference
    diff = np.diff(pts, axis=1)
    rect[1] = pts[np.argmin(diff)]
    rect[3] = pts[np.argmax(diff)]
    # return the ordered coordinates
    return rect


