#!/bin/bash

# remove all containers
docker rm $(docker ps -aq)

# remove all images
docker rmi -f $(docker images -a | grep '<none>' | awk '{print $3}')
