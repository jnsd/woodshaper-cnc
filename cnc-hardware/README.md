# Building a Shapeoko-based CNC

Everything needed to build a Shapeoko 2 based CNC machine.

## Grbl Setup

### Machine Zero After Homing

To set machine zero (x=0,y=0,z=0) right where the machine stops after homing, you need to tweak some configuration, compile Grbl and flash the Arduino:

1. Get the Grbl repo `git clone https://github.com/gnea/Grbl.git`
2. Uncomment ` #define HOMING_FORCE_SET_ORIGIN` in `config.h` or use the `Grbl/config.h`  file in this repo
3. Compile and flash Grbl using Arduino IDE (make sure to copy the files to the correct location): https://github.com/gnea/Grbl/wiki/Compiling-Grbl

### Grbl Config





- `$1=255` >> Keep motors constantly activated, i.e. not moveable by manual force
- `$3=4` >> to switch z movement direction
- `$20=1` >> Activate soft limits
- `$21=1` >> Activate hard limits
- `$22=1` >> Require homing when switching on machine
- `$23=3` >> Direction where to search for hard limit switches
- `$25=1000` >> Speed for seeking the hard limits
- `$27=5` >> 5 mm pull-off after homing
- `$100=101=417` >> steps/mm for x and y axes
- `$102=600` >> steps/mm for z axis
- `$110=$111=1600` >> max speed for x and y axes
- `$112=500` >> max speed for z axis
- `$120=$121=7` >> acceleration for x and y axes
- `$122=10` >> acceleration for z axis
- `$130=1000` >> max cnc area in x direction, i.e. left-right
- `$131=750` >> max cnc area in y direction, i.e. front-back
- `$132=200` >> any value to allow moving in z direction

### Machine Startup Scripts

We need to configure the following script as startup block:`$N0=G21 G90 G17`

The script will...

- set units to be in mm
- set absolute coordinates
- select XY plane

every time the machine is...

- powered up
- reset
- homed (only if homing is enabled with `$22=1`)

## How To Start The Machine

**DO THIS EVERY TIME THE MACHINE IS SWITCHED ON!!!**

1. Before powering up the machine: move the x axis gantry towards the front until it touches the end stop screws. Keep the gantry in tis position!
2. Power up the machine which will activate the motors and keep the gantry in place, i.e. perpendicular to the y axis beams. You can now release the grip on the x axis gantry.
3. Run the homing cycle.

## AMB 1050 FME-1 Portal Fräse

Techniker bei AMB: Mr. Beutelsbacher

- Die Steuerleitung muss von der Hochvolt-Leitung (230V) galvanisch getrennt werden
- Die galvanische Trennung wird durch den konstanten 10-32V Dauerstrom umgesetzt
- Ohne galvanische Trennung kann in speziellen Fällen die Hochvolt-Spanung (230V) auf die Steuerleitung überspringen. Dies wird bei Bastellösungen oft vernachlässigt.
- Die Steuerleitung kann mit analog 0-10V oder PWM-Signal gesteuert werde vorausgesetzt, dass die Frequenz hoch genug ist. Hr. Beutelsbacher meinte, dass 1kHz ausreichend sei.

## Sources

- [Control High Voltage Devices with an Arduino and a Relay](http://howtomechatronics.com/tutorials/arduino/control-high-voltage-devices-arduino-relay-tutorial/)

