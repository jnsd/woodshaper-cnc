# sources:
# https://oscarliang.com/connect-raspberry-pi-and-arduino-usb-cable/
# http://techtinkering.com/2013/04/02/connecting-to-a-remote-serial-port-over-tcpip/


# go to home folder
cd ~

# update raspbian os
sudo apt-get update
sudo apt-get dist-upgrade

# install git
sudo apt-get -qy install git


# OPTIONAL:
# install docker
# sudo curl -sSL https://get.docker.com | sh
# Know issue in Docker 17.11.0:
# "docker: Error response from daemon: cgroups: memory cgroup not supported on this system: unknown"
# downgrade to docker version that works on RPi if you get the following error:
# sudo apt-get -qy install docker-ce=17.09.0~ce-0~raspbian


# install package managers for python 2 & 3
sudo apt-get -qy install python3-setuptools
sudo python2 -m easy_install pip
sudo python3 -m easy_install pip

# install serial port lib for python 2 & 3
sudo pip2 install pyserial
sudo pip3 install pyserial

# install GPIO python libs for RPi
sudo pip2 install rpi.gpio
sudo pip3 install rpi.gpio


mkdir /home/pi/apps
cd /home/pi/apps
git clone --recursive https://github.com/xpix/chilipeppr.local
git clone https://github.com/chilipeppr-grbl/widget-grbl.git
git clone https://github.com/chilipeppr-grbl/widget-grbl-xyz.git
git clone https://github.com/chilipeppr-grbl/widget-grbl-touchplate.git
git clone https://github.com/chilipeppr-grbl/widget-grbl-autolevel.git

sudo apt-get install nginx
sudo /etc/init.d/nginx start
cd /var/www/html
sudo ln -s /home/pi/apps/chilipeppr.local .


# Option A "using SPJS":
#   run serial-port-json-server on RPi and connect to it using Chilipeppr
# Option B "using a virtual serial port":
#   figure out the Arduino serial port using "ls /dev/tty*"
#   add the line "3333:raw:0:/dev/<Arduino port, e.g. /ev/ttyACM0>:115200 8DATABITS NONE 1STOPBIT" to /etc/ser2net.conf
#   start ser2net
#   start socat on client "socat pty,link=$HOME/ttycnc tcp:<RPi name or IP>:3333" (creates virtual serial port that pipes through tcp to serial port on RPi)

############ Option A ###############
# download serial-port-json-server to allow connecting to the serial port via web socket
# mkdir apps
# cd apps
# wget http://chilipeppr.com/downloads/v1.80/serial-port-json-server_1.80_linux_armv6.tar.gz
# tar -xvf serial-port-json-server_1.80_linux_armv6.tar.gz

############ Option B ###############
# install, configure and start tool to link serial port to tcp
sudo apt-get -qy install ser2net
sudo mv /etc/ser2net.conf /etc/ser2net.conf_bk
# change the Arduino serial port /dev/ttyACM0 on the RPi if necessary (check using "ls /dev/tty*"):
echo "3333:raw:0:/dev/ttyACM0:115200 8DATABITS NONE 1STOPBIT" | sudo tee -a /etc/ser2net.conf
ser2net


