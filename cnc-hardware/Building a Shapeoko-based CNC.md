# Building a Shapeoko-based CNC

## AMB 1050 FME-1 Portal Fräse

Techniker bei AMB: Mr. Beutelsbacher

- Die Steuerleitung muss von der Hochvolt-Leitung (230V) galvanisch getrennt werden
- Die galvanische Trennung wird durch den konstanten 10-32V Dauerstrom umgesetzt
- Ohne galvanische Trennung kann in speziellen Fällen die Hochvolt-Spanung (230V) auf die Steuerleitung überspringen. Dies wird bei Bastellösungen oft vernachlässigt.
- Die Steuerleitung kann mit analog 0-10V oder PWM-Signal gesteuert werde vorausgesetzt, dass die Frequenz hoch genug ist. Hr. Beutelsbacher meinte, dass 1kHz ausreichend sei.

## Sources

- [Control High Voltage Devices with an Arduino and a Relay](http://howtomechatronics.com/tutorials/arduino/control-high-voltage-devices-arduino-relay-tutorial/)

