# Raspberry Pi Serial Port Bridge

Allows to use an RPi as a bridge for serial ports, e.g. with an Arduino attached to the RPi's USB:

    CNC  <== GPIO ==>  Arduino  <== usb ==>  RPi  <== serial over tcp ==>  client computer

The serial port on the RPi is linked to a tcp port. This tcp port can be accessed remotely, e.g. over wifi. [ser2net](http://ser2net.sourceforge.net/) is used to connect the serial port to a tcp port. On the client, create a virtual serial port with [socat](http://www.dest-unreach.org/socat/). Now you can connect to the RPi's serial port (e.g. Arduino) over wifi and tcp.


## Sources

[Connect Raspberry Pi and Arduino with Serial USB Cable](https://oscarliang.com/connect-raspberry-pi-and-arduino-usb-cable/)

[Connecting to a Remote Serial Port over TCP/IP](http://techtinkering.com/2013/04/02/connecting-to-a-remote-serial-port-over-tcpip/)