# run this script on RPi after installing raspbian os

# sources:
# https://oscarliang.com/connect-raspberry-pi-and-arduino-usb-cable/
# http://techtinkering.com/2013/04/02/connecting-to-a-remote-serial-port-over-tcpip/


# update raspbian os
sudo apt-get update
sudo apt-get dist-upgrade

# install, configure and start tool to link serial port to tcp
sudo apt-get -qy install ser2net
sudo mv /etc/ser2net.conf /etc/ser2net.conf_bk
# get the Arduino serial port on the RPi using "ls /dev/tty*"
echo "3333:raw:0:/dev/ttyACM0:115200 8DATABITS NONE 1STOPBIT" | sudo tee -a /etc/ser2net.conf
ser2net

# TODO: make ser2net start on RPi boot

# Setup client computer:
# start socat "socat pty,link=$HOME/ttycnc tcp:<RPi name or IP>:3333" (creates virtual serial port that pipes through tcp to serial port on RPi)
